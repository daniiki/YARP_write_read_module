# Installation
## Dependencies
- [YARP and iCub software](http://wiki.icub.org/wiki/Linux:Installation_from_sources)
- [Gazebo and plugins](http://robotology.gitlab.io/docs/gazebo-yarp-plugins/master/install.html)
- [HeiCub simulation models](https://bitbucket.org/yue_hu/icubheidelberg01)
- [codyco-superbuild](https://github.com/robotology/codyco-superbuild)
- [yaml-cpp](https://github.com/jbeder/yaml-cpp)

## Compilation
    https://orb.iwr.uni-heidelberg.de/code-new/software/heicub/YARP_write_read_module
    cd YARP_write_read_module
    mkdir build
    cd build
    cmake ..
    make
    
# Configuration
We are using YAML to specify the configuration.
There are two sections - a read and a write section.
The former is used for reading data from robot sensors and
writing the data to a file or a YARP port.
In the latter, you can specify files and YARP ports
from which commands will be read periodically
and passed on to the robot's motors.

## Read section
It is necessary to specify the following for each entry in the read section:

- `file` and/or `port`: The sensor data will be written to this location.
  When using a port, the data will be sent as
  [Vector](http://yarp.it/classyarp_1_1sig_1_1Vector.html)s.
  When using a file, the data will be saved as comma-separated values
  with one line written every `period` ms.
- `period`: The rate in milliseconds at which sensor data is read.
- `unit`: One of `degree` or `radian`. If left out, `degree` is used.
  If set to `radian` data is converted to radians, i.e. multiplied with PI/180. 
  Do NOT set this option if you are reading from a sensor that returns 
  neither of position, speed or acceleration data,
  since data is multiplied with the constant regardless of the sensor type.
- `sensors`: The list of sensors to read data from, as described below.

Each entry of the `sensors` list specifies that data should be read from either 
certain joints or certain YARP ports.
If data is read from joints the entry must provide the following information:

- `part`: The robot part the joints belong to, e.g. `left_leg`.
- `type`: The type of the sensor. One of the following:
  - `encoder_pos`, which uses
    [IEncoders::getEncoders()](http://yarp.it/classyarp_1_1dev_1_1IEncoders.html#abcfe10041280b99c7c4384c4fd93a9dd)
  - `encoder_accel`, which uses
    [IEncoders::getEncoderAccelerations()](http://yarp.it/classyarp_1_1dev_1_1IEncoders.html#a3bcb5fe5c6a5e15e57f4723bbe12c57a)
  - `encoder_vel`, which uses
    [IEncoders::getEncoderSpeeds()](http://yarp.it/classyarp_1_1dev_1_1IEncoders.html#ac84ae2f65f4a93b66827a3a424d1f743)
  - `motor_pos`, which uses
    [IMotorEncoders::getEncoders()](http://yarp.it/classyarp_1_1dev_1_1IMotorEncoders.html#ac02fb05bb3e9ac9a381d41b59c38c412)
  - `motor_accel`, which uses
    [IMotorEncoders::getMotorEncoderAccelerations()](http://yarp.it/classyarp_1_1dev_1_1IMotorEncoders.html#a9394d8b5cc4f3d58aeaa07c3fb9a6e6a)
  - `motor_vel`, which uses
    [MotorEncoders::getMotorEncoderSpeeds()](http://yarp.it/classyarp_1_1dev_1_1IMotorEncoders.html#a4624348cc129bfeb12ad0e6d2892b76c)
  - `torque`, which uses
    [ITorqueControl::getRefTorques()](http://yarp.it/classyarp_1_1dev_1_1ITorqueControl.html#a06de1f7ea05944c16483efda252bd2b3)
  - `current`, which uses
    [ICurrentControl::getCurrents()](http://yarp.it/classyarp_1_1dev_1_1ICurrentControl.html#a00aef7aa5c0d0da111cdf19ab3bad630)
  - `voltage`, which uses
    [IPWMControl::getDutyCycles()](http://yarp.it/classyarp_1_1dev_1_1IPWMControl.html#a9f1ba2bcdc5709a3e3e0aafa69e77bd7)
- `joint` or `joints`: A single joint or a list of joints. 
  If the part hast n+1 joints this is a number/list of numbers between 0 and n.

Data can also be read from a YARP port.
This is useful to access information about

- Inertial Measurement Unit (IMU), available at `/$robotName/inertial`
- Force Torque sensors, available at `/$robotName/left_foot/analog:o` and
  `/$robotName/left_leg/analog:o` and equivalent for the right side

We assume that data is written to the port in the form of a
[Vector](http://yarp.it/classyarp_1_1sig_1_1Vector.html).
In this case the entry must provide the following information:

- `port`: The port to read data from, e.g. `/icub/inertial` or `/icub/right_leg/analog:o`.
- `index` or `indices`: The index or a list of indices to take from the arriving vectors.
  If the vectors that are written to the port have n+1 entries
  this is a number/list of numbers between 0 and n.

The order of the different sensors' data in the output
is the same as specified in the configuration file.

## Write section
It is necessary to specify the following for each entry in the write section:

- `file` or `port`: The file or the port you read commands from.
  When using a port, the vectors received are queued.
  <!-- Is this really the case when using a buffered Port?-->
  When using a file, the next line is read every `period` milliseconds.
  Each line should contain exactly n values,
  where n is the number of motors specified.
  Values must be separated by commas.
  For example, a file could look like this if you want to move two motors:
  
  ```
  -0.00613448,   -0.00678458
  -0.00603466,   -0.00674605
   0.0060988,    -0.00672312
   0.00607314,   -0.00672804
   0.00610039,   -0.00674522
   ...            ...
  ```
  
- `period`: Every `period` milliseconds the next command is executed.
- `mode`: The control mode to use. One of the following:
  - `position`, which uses
    [IPositionControl2::positionMove()](http://yarp.it/classyarp_1_1dev_1_1IPositionControl2.html#af6b76fc7346c1a53adad7291d4e1b1bb)
    (Before sending the given position data to the robot,
    the reference speed will automatically be set such that
    the given position is reached within `period` milliseconds.)
  - `position_direct`, which uses
    [IPositionDirect::setPositions()](http://yarp.it/classyarp_1_1dev_1_1IPositionDirect.html#a9eb093e4ab1e2684d9059981b8e0519c)
  - `velocity`, which uses
    [IVelocityControl2::velocityMove()](http://yarp.it/classyarp_1_1dev_1_1IVelocityControl2.html#a1c2f3119cfb768bbe288aa4c9f60dbfe)
  - `torque`, which uses
    [ITorqueControl::setRefTorques()](http://yarp.it/classyarp_1_1dev_1_1ITorqueControl.html#ad2e16c7734f445bbb233ceb98fadf3ee)
  - `pwm`, which uses
    [IPWMControl::setRefDutyCycle()](http://yarp.it/classyarp_1_1dev_1_1IPWMControl.html#a1ec7e938fc5fc3b1b0446010e5b4caf6)
- `unit`: One of `degree` or `radian`. If left out, `degree` is used.
  If set to `radian` commands are converted from radians to degrees, i.e. multiplied with 180/PI. 
  Do NOT set this option if you are using PWM control,
  since data is multiplied with the constant regardless of the control mode.
- `initial_velocity`: If set, the robot will move to the initial position at this velocity,
  where the initial position is the one specified on the first line of the input file
  or the first vector received by the port.
  The movement will happen using `IPositionControl2`.
  After the initial position is reached, the first line/first vector will be repeated,
  but using the control mode specified in `mode` this time.
  The velocity has to be given in degrees per second or radians per second, depending on `unit`.
  Do NOT use this option with a control mode other than `position` and `position_direct`,
  as only these encode position data.
- `motors`: The list of motors to send commands to, as described below.

Each entry of the `motors` list must provide the following information:

- `part`: The robot part the joints belong to, e.g. left_leg.
- `joint` or `joints`: A single joint or a list of joints. 
  If the part hast n+1 joints this is a number/list of numbers between 0 and n.

## Example configuration file
```yaml
read:
  - file: left_encoders.csv
    period: 10
    unit: radian
    sensors:
      - part: left_leg
        type: encoder_pos
        joints: [2, 3]
  - port: /data/encoder
    file: right_encoders.csv
    period: 10
    sensors:
      - part: right_leg
        type: encoder_vel
        joint: 0
      - port: /inertial
        indices: [2,3]

write:
  - port: /motor/right_leg
    mode: position
    unit: degree
    period: 10
    motors:
      - part: right_leg
        joints: [4, 5]
      - part: torso
        joint: 0
      - part: right_leg
        joint: 0
  - file: icub_walk_seq_left.csv
    period: 10
    unit: radian
    mode: position_direct
    initial_velocity: 0.5
    motors:
        - part: left_leg
          joints: [0,1,2,3,4,5]
```
In this case, `icub_walk_seq_left.csv` should contain exactly six values per line.

# Running the program
If you're using Gazebo it makes sense to
[syncronise the clocks](http://robotology.gitlab.io/docs/gazebo-yarp-plugins/master/classgazebo_1_1GazeboYarpClock.html).
For short, start Gazebo using

    gazebo -slibgazebo_yarp_clock.so 
    
and before running the program set 

    YARP_CLOCK=/clock
    
The program consists of two parts. To read sensor data use

    ./read --robot $robotName
    
where `$robotName` is the name of your robot, e.g. `icubGazeboSim`.

To move the motors use 

    ./write --robot $robotName
    
If your configuration file is not located at the default `configuration.yaml`, 
you can use `--config` to specify a different file.

# RPC commands
You can pause and unpause the `write` and `read` programs using YARP ports.
To do this, open any port on the commandline, e.g.

    yarp write /command

Then, connect it to `/write/rpc` (or `/read/rpc`) using

    yarp connect /command /write/rpc

and write the words `stop` or `start` to the console, followed by `<Return>`.

If you want to use this RPC interface programatically,
just make sure to send `Bottle`s with the respective content to `/write/rpc`.

It is also possible to only begin reading sensor data as soon as the movement begins.
For that, you first have to start `read` with the `--wait-for-rpc` flag.
Then, you can run `write --start-read`,
which will additionally stop the reading process when the end of an input file is reached.
Therefore, it should only be used with a single input.
When using `--wait-for-rpc`, the RPC commands can also be used to manually start reading.
This will ensure that the first line of the output fiĺe
is only written as soon as `start` is sent.
    
# Documentation
You can find the documentation of the classes [here](https://daniiki.gitlab.io/YARP_write_read_module/annotated.html).
For the YARP classes used refer to [their documentation](http://yarp.it/).
yaml-cpp is documented in a [tutorial](https://github.com/jbeder/yaml-cpp/wiki/Tutorial).
