#include <cstdio>
#include <yarp/os/Network.h>
#include <yarp/dev/ControlBoardInterfaces.h>
#include <yarp/dev/PolyDriver.h>
#include <yarp/os/Time.h>
#include <yarp/sig/Vector.h>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <chrono>
#include <yarp/os/RateThread.h>
#include <signal.h>
#include <stdlib.h>
#include <variant>
#include <mutex>
#include <math.h>

#include "yaml-cpp/yaml.h"

#define DEG2RAD M_PI/180.0

using namespace std;
using namespace yarp::dev;
using namespace yarp::sig;
using namespace yarp::os;

class LastReadPort;

// Map: portname -> LastReadPort
std::map<std::string, LastReadPort*> portsensor;

// Map: partname -> driver 
std::map<std::string, PolyDriver*> robotDevice;
std::map<std::string, IEncoders*> enc;
std::map<std::string, IMotorEncoders*> motor_enc;
std::map<std::string, ITorqueControl*> torque_enc;
std::map<std::string, ICurrentControl*> current_enc;
std::map<std::string, IPWMControl*> voltage_enc;

typedef std::string partname;
typedef std::string type;
typedef size_t vector_index;
typedef size_t joint_num;
typedef std::string portname;
typedef size_t port_index;

/**
 * A sensor that belongs to a certain joint.
 */
struct JointSensor
{
    std::string part; ///< e.g "left_leg"
    size_t joint; ///< If the part has n+1 joints, this is a number between 0 and n.
    std::string type; ///< e.g. "encoder_pos", "motor_accel", ... 
};

/**
 * A sensor that simply reads data from a given port.
 */
struct PortSensor
{
    std::string portname; ///< e.g. "/inertial"
    size_t index; ///< Index in Vector that was read from the port.
};

/**
 * A Port that stores the value last received. 
 * It can be retrieved using lastRead().
 */
class LastReadPort : public Port
{
public:
    LastReadPort()
    {
        setCallbackLock(&mutex); // Mutex is locked before writing into last
        setReader(last); // Automatically write data that is received into last
    }
    
    /**
     * @return Last Vector that was received.
     */
    Vector lastRead()
    {
        lockCallback();
        auto tmp = last;
        unlockCallback();
        return tmp;
    }

private:
    Vector last; ///< Last Vector that was received
    Mutex mutex; ///< Used to prevent the callback from writing to `last` while it's read from.
};

/**
 * At the rate given by `period`, read data from robot sensors.
 * After data has been read, the virtual action() method is called.
 * RateThread provides a start() method that has to be called.
 */
class Output : public RateThread
{
public:
    /**
     * @param sensors Vector that stores JointSensor and PortSensor objects in the order
     *                in which they appear in the config file.
     * @param use_radian_ Whether to use radian instead of degree.
     *                    If set to true, the output will be converted from degrees to radians.
     *                    Don't set to true when using any sensor that doesn't output position, speed or acceleration.
     * @param period Rate in milliseconds at which the run() method is called.
     */
    Output(std::vector<std::variant<JointSensor, PortSensor>> sensors, bool use_radian_, int period) : v(sensors.size()), use_radian(use_radian_), RateThread(period)
    {
        // fill `parts` and `ports` maps
        for (size_t i = 0; i < sensors.size(); ++i)
        {
            if (std::holds_alternative<JointSensor>(sensors[i]))
            {
                JointSensor& j = std::get<JointSensor>(sensors[i]);
                // joint appears at ith position in config file
                parts[j.part][j.type].push_back({i, j.joint});
            }
            else
            {
                PortSensor& p = std::get<PortSensor>(sensors[i]);
                ports[p.portname].push_back({i, p.index});
            }
        }
    }
    
    /**
     * Read data from sensors that were specified in `sensors` and call action().
     * This method is called every `period` ms by RateThread.
     */
    virtual void run()
    {
        bool ok = true;
        for (auto& entry : parts)
        {
            auto& part = entry.first;
            auto& types = entry.second;
            Vector encoders;
            Vector motor_encoders;
            Vector torques;
            Vector currents;
            Vector voltages;
            for (auto& entry : types)
            {
                auto& type = entry.first;
                auto& joints_in_part = entry.second;
                if (type.substr(0, 7) == "encoder")
                {
                    if (encoders.size() == 0)
                    {
                        int ax = 0;
                        ok = ok && enc[part]->getAxes(&ax);
                        // encoders will hold the data of every joint belonging to the part
                        encoders.resize(ax); 
                    }
                    
                    if (type == "encoder_pos")
                    {
                        ok = ok && enc[part]->getEncoders(encoders.data());
                    }
                    else if (type == "encoder_accel")
                    {
                        ok = ok && enc[part]->getEncoderAccelerations(encoders.data());
                    }
                    else if (type == "encoder_vel")
                    {
                        ok = ok && enc[part]->getEncoderSpeeds(encoders.data());
                    }
                    
                    for (auto& joint : joints_in_part)
                    {
                        v[joint.first] = encoders[joint.second];
                    }
                }
                else if (type.substr(0, 5) == "motor")
                {
                    if (motor_encoders.size() == 0)
                    {
                        int num = 0;
                        ok = ok && motor_enc[part]->getNumberOfMotorEncoders(&num);
                        motor_encoders.resize(num);
                    }
                    
                    if (type == "motor_pos")
                    {
                        ok = ok && motor_enc[part]->getMotorEncoders(motor_encoders.data());
                    }
                    else if (type == "motor_accel")
                    {
                        ok = ok && motor_enc[part]->getMotorEncoderAccelerations(motor_encoders.data());
                    }
                    else if (type == "motor_vel")
                    {
                        ok = ok && motor_enc[part]->getMotorEncoderSpeeds(motor_encoders.data());
                    }
                    
                    for (auto& joint : joints_in_part)
                    {
                        v[joint.first] = motor_encoders[joint.second];
                    }
                }
                else if (type == "torque")
                {
                    if (torques.size() == 0)
                    {
                        int ax = 0;
                        ok = ok && torque_enc[part]->getAxes(&ax);
                        torques.resize(ax);
                    }
                    
                    ok = ok && torque_enc[part]->getTorques(torques.data());
                    
                    for (auto& joint : joints_in_part)
                    {
                        v[joint.first] = torques[joint.second];
                    }
                }
                else if (type == "current")
                {
                    if (currents.size() == 0)
                    {
                        int ax = 0;
                        ok = ok && current_enc[part]->getNumberOfMotors(&ax);
                        currents.resize(ax);
                    }
                    
                    ok = ok && current_enc[part]->getCurrents(currents.data());
                    
                    for (auto& joint : joints_in_part)
                    {
                        v[joint.first] = currents[joint.second];
                    }
                }
                else if (type == "voltage")
                {
                    if (voltages.size() == 0)
                    {
                        int ax = 0;
                        ok = ok && voltage_enc[part]->getNumberOfMotors(&ax);
                        voltages.resize(ax);
                    }
                    
                    ok = ok && voltage_enc[part]->getDutyCycles(voltages.data());
                    
                    for (auto& joint : joints_in_part)
                    {
                        v[joint.first] = voltages[joint.second];
                    }
                }
            }
        }
        if (!ok)
        {
            std::cerr << "Could not read sensor data." << std::endl;
            //std::exit(1);
        }
        
        // read ports that appeared in the PortSensors contained in sensors
        for (auto& entry : ports)
        {
            auto& portname = entry.first;
            Vector read = portsensor[portname]->lastRead();
            
            for (auto& index : entry.second)
            {
                // index.first = index according to order in config file
                // index.second = index to read from port (specified in PortSensor)
                v[index.first] = read[index.second];
            }
        }
        
        if (use_radian)
        {
            for (size_t i = 0; i < v.size(); ++i)
            {
                v[i] = v[i] * DEG2RAD;
            }
        }
        
        action(v);
    }
    
    /**
     * Write sensor data to file/port.
     * This method is called by run().
     * It is purely virtual and must therefore be implemented by a child class.
     */
    virtual void action(Vector v) = 0;

private:
    /**
     * map: partname -> pair<index in Vector v (according to order in config), joint>
     */
    std::map<partname, std::map<type, std::vector<std::pair<vector_index, joint_num>>>> parts;
    /**
     * map: portname -> pair<index in Vector v, index in portvector>
     */
    std::map<portname, std::vector<std::pair<vector_index, port_index>>> ports;
    Vector v; ///< Vector that stores the data to pass to action().
    bool use_radian; ///< Whether to use radian instead of degree
};

/**
 * Periodically write sensor data to file.
 */
class OutputFile : public Output
{
public:
    /**
     * @param filename file to write to
     * @param sensors Vector that stores JointSensor and PortSensor objects in the order
     *                in which they appear in the config file.
     * @param use_radian Whether to use radian instead of degree.
     *                   If set to true, the output will be converted from degrees to radians.
     *                   Don't set to true when using any sensor that doesn't output position, speed or acceleration.
     * @param period Rate in milliseconds at which the run() method is called.
     */
    OutputFile(std::string filename, std::vector<std::variant<JointSensor, PortSensor>> sensors, bool use_radian, int period) : stream(filename), Output(sensors, use_radian, period)
    {
        if (!stream.is_open())
        {
            std::cerr << "Could not open file " << filename << std::endl;
            std::exit(1);
        }
    }
    
    /**
     * Write data to file. This is called every `period` ms by run().
     */
    virtual void action(Vector v)
    {
        for (size_t i = 0; i < v.size() - 1; ++i)
        {
            stream << v[i] << ",";
        }
        stream << v[v.size() - 1] << "\n";
    }
    
    std::ofstream stream; ///< Ouput stream of file to write data to.
};

/**
 * Periodically write sensor data to port.
 */
class OutputPort : public Output
{
public:
    /**
     * @param portname ports to write to
     * @param sensors Vector that stores JointSensor and PortSensor objects in the order
     *                in which they appear in the config file.
     * @param use_radian Whether to use radian instead of degree.
     *                   If set to true, the output will be converted from degrees to radians.
     *                   Don't set to true when using any sensor that doesn't output position, speed or acceleration.
     * @param period Rate in milliseconds at which the run() method is called.
     */
    OutputPort(std::string portname, std::vector<std::variant<JointSensor, PortSensor>> sensors, bool use_radian, int period) : Output(sensors, use_radian, period)
    {
        port.open(portname);
        if (port.isClosed())
        {
            std::cerr << "Could not open port " << portname << std::endl;
            std::exit(1);
        }
    }
    
    /**
     * Write data to port. This is called every `period` ms by run().
     */
    virtual void action(Vector v)
    {
        Vector& data = port.prepare();
        data = v;
        port.write();
    }

    BufferedPort<Vector> port; ///< Port to write data to.
};

std::vector<OutputFile*> files;
std::vector<OutputPort*> ports;

class RpcPort : public BufferedPort<Bottle>
{
public:
    RpcPort()
    {
        useCallback();
    }
    
    using BufferedPort<Bottle>::onRead;
    virtual void onRead(Bottle& b)
    {
        std::string cmd = b.toString();
        std::cout << cmd << std::endl;
        if (cmd == "stop")
        {
            for (auto& port : ports)
            {
                port->stop();
            }
            for (auto& file : files)
            {
                file->stop();
            }
        }
        else if (cmd == "start")
        {
            for (auto& port : ports)
            {
                port->start();
            }
            for (auto& file : files)
            {
                file->start();
            }
        }
        else
        {
            std::cerr << "Unknown RPC command: " << cmd << std::endl;
        }
    }
};

/**
 * This is called when Ctrl-C is pressed.
 * It closes all ports and streams and then exits.
 */
void my_handler(int s)
{
    printf("Caught signal %d\n",s);
    for (auto& port : ports)
    {
        port->stop();
        port->port.close();
    }
    for (auto& file : files)
    {
        file->stop();
        file->stream.close();
    }
    std::exit(EXIT_SUCCESS); 
}

/**
 * - Read --robot command line argument
 * - Read read section of config file and create OuputPort and OuputFile objects
 * - Set up IEncoders, ... objects
 * - Start RateThreads
 */
int main(int argc, char *argv[])
{
    Network yarp;
    Property params;
    params.fromCommand(argc, argv);
    
    if (!params.check("robot"))
    {
        std::cerr << "Please specify the name of the robot" << std::endl;
        std::cerr <<"--robot name (e.g. icub)" << std::endl;
        std::exit(1);
    }
    std::string robotName = params.find("robot").asString().c_str();
    
    std::string configName;
    if (params.check("config"))
        configName = params.find("config").asString().c_str();
    else
        configName = "configuration.yaml";
    YAML::Node config;
    try
    {
        config = YAML::LoadFile(configName);
    }
    catch (YAML::BadFile e)
    {
        std::cerr << "Could not open file " << configName << std::endl;
        std::exit(1);
    }
    
    bool wait_for_rpc = false; // start execution as soon as start RPC command is received
    if (params.check("wait-for-rpc"))
        wait_for_rpc = true;
    
    // store for each part the types we need to access (encoders, motor encoders, ...)
    std::map<partname, std::set<type>> accessed_types;
    std::set<std::string> accessed_ports;
    
    // read config
    for (YAML::const_iterator read = config["read"].begin();
         read != config["read"].end();
         ++read)
    {
        bool use_radian = false;
        if ((*read)["unit"])
        {
            std::string unit = (*read)["unit"].as<std::string>();
            if (unit == "radian")
                use_radian = true;
            else if (unit == "degree")
                use_radian = false;
            else
            {
                std::cerr << "Unknown unit: " << unit << std::endl;
                std::exit(1);
            }
        }
        std::vector<std::variant<JointSensor, PortSensor>> sensors;
        // add joints to joints vector
        for (YAML::const_iterator sensor = (*read)["sensors"].begin();
             sensor != (*read)["sensors"].end();
             ++sensor)
        {
            if ((*sensor)["port"]) // inertial and force torque sensors 
            {
                accessed_ports.insert((*sensor)["port"].as<std::string>());
                if ((*sensor)["index"]) // index
                {
                    sensors.push_back(PortSensor{
                        (*sensor)["port"].as<std::string>(), 
                        (*sensor)["index"].as<size_t>()
                    });
                }
                else // indices: [...]
                {
                    for (YAML::const_iterator index = (*sensor)["indices"].begin();
                        index != (*sensor)["indices"].end();
                        ++index)
                    {
                        sensors.push_back(PortSensor{
                            (*sensor)["port"].as<std::string>(), 
                            index->as<size_t>()
                        });
                    }
                }
            }
            else // joint sensors
            {
                std::string type = (*sensor)["type"].as<std::string>();
                std::string part = (*sensor)["part"].as<std::string>();
                accessed_types[part].insert(type);
                if ((*sensor)["joint"]) // single joint
                {
                    sensors.push_back(JointSensor{
                        part,
                        (*sensor)["joint"].as<size_t>(),
                        type
                    });
                }
                else // joints: [ ... ]
                {
                    for (YAML::const_iterator joint = (*sensor)["joints"].begin();
                        joint != (*sensor)["joints"].end();
                        ++joint)
                    {
                        sensors.push_back(JointSensor{
                            part,
                            joint->as<size_t>(),
                            type
                        });
                    }
                }
            }
        }
        
        // create OutputFile and OutputPort objects
        if ((*read)["file"])
        {
            auto file = new OutputFile(
                (*read)["file"].as<std::string>(), 
                sensors, use_radian, (*read)["period"].as<int>());
            
            files.push_back(file);
        }
        if ((*read)["port"])
        {
            auto port = new OutputPort(
                (*read)["port"].as<std::string>(),
                sensors, use_radian, (*read)["period"].as<int>());

            ports.push_back(port);
        }
    }
    
    for (auto& port : accessed_ports)
    {
        auto p = new LastReadPort;
        p->open("/client" + port);
        Network::connect(port, "/client" + port);
        portsensor[port] = p;
    }
    
    for (auto& entry : accessed_types)
    {
        auto& part = entry.first;
        auto& types = entry.second;
        
        std::string remotePorts = "/" + robotName + "/" + part;
        std::string localPorts = "/client/" + part;
        Property options;
        options.put("device", "remote_controlboard");
        options.put("local", localPorts.c_str());   //local port names
        options.put("remote", remotePorts.c_str());
        
        robotDevice[part] = new PolyDriver(options);
        if (!robotDevice[part]->isValid()) 
        {
            std::cerr << "Device not available. Here are the known devices:" << std::endl;
            std::cout << Drivers::factory().toString().c_str();
            std::exit(1);
        }
        bool ok = true;
        // check whether types contains encoder_* entry
        // if that's the case, create an IEncoders object 
        if (types.find("encoder_pos") != types.end() || types.find("encoder_accel") != types.end() || types.find("encoder_vel") != types.end())
        {
            IEncoders* e;
            ok = ok && robotDevice[part]->view(e);
            enc[part] = e;
        }
        if (types.find("motor_pos") != types.end() || types.find("motor_accel") != types.end() || types.find("motor_vel") != types.end())
        {
            IMotorEncoders* m;
            ok = ok && robotDevice[part]->view(m);
            motor_enc[part] = m;
        }
        if (types.find("torque") != types.end())
        {
            ITorqueControl* t;
            ok = ok && robotDevice[part]->view(t);
            torque_enc[part] = t;
        }
        if (types.find("current") != types.end())
        {
            ICurrentControl* c;
            ok = ok && robotDevice[part]->view(c);
            current_enc[part] = c;
        }
        if (types.find("voltage") != types.end())
        {
            IPWMControl* p;
            ok = ok && robotDevice[part]->view(p);
            voltage_enc[part] = p;
        }
        if (!ok) 
        {
            std::cout << "Problems acquiring interfaces" << std::endl;
            std::exit(1);
        }
    }
    
    // call my_handler() when a SIGINT signal is received
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    
    if (!wait_for_rpc)
    {
        // start RateThreads
        for (auto& port : ports)
        {
            port->start();
        }
        for (auto& file : files)
        {
            file->start();
        }
    }
    
    RpcPort r;
    r.open("/read/rpc");
    
    // pause main thread until an interrupt is received
    pause();
}
