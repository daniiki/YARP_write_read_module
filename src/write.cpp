#include <cstdio>
#include <yarp/os/Network.h>
#include <yarp/dev/ControlBoardInterfaces.h>
#include <yarp/dev/PolyDriver.h>
#include <yarp/os/Time.h>
#include <yarp/sig/Vector.h>
#include <fstream>
#include <yarp/os/RateThread.h>
#include <math.h>
#include "yaml-cpp/yaml.h"

#define RAD2DEG 180.0/M_PI
#define NO_INITIAL_VEL 0.0

using namespace std; 
using namespace yarp::dev;
using namespace yarp::sig;
using namespace yarp::os;

// map: partname -> driver
std::map<std::string, IControlMode2*> controlmode;
std::map<std::string, IEncoders*> enc;
std::map<std::string, IPositionControl2*> pos; 
std::map<std::string, IPositionDirect*> posdirect;
std::map<std::string, IVelocityControl2*> vel;
std::map<std::string, ITorqueControl*> torque;
std::map<std::string, IPWMControl*> pwm;

Port readRPC;
bool start_read;
bool stop_read;

/**
 * Status of movement to initial position.
 */
enum InitialPosStatus { NOT_STARTED, MOVING, REPEAT_FIRST_POSITION, DONE };

/**
 * a motor is located at a joint
 */
struct Joint
{
    std::string part; ///< e.g "left_leg"
    size_t joint; ///< if the part has n+1 joints, this is a number between 0 and n
};

/**
 * At the rate given by `period_`, read a command in the form of a Vector and - 
 * depending on the control mode - move the motors specified in `joints` accordingly.
 * RateThread provides a start() method that has to be called.
 */
class Input : public RateThread
{
public:
    /**
     * @param joints The joints to move when action() is called. They should be in the same
     *               order as in the config file.
     * @param mode_ Constant representing control mode.
     * @param use_radian_ Whether to use radian instead of degree.
     *                    If set to true, the input will be converted from degrees to radians.
     *                    Makes no sense when using PWM control.
     * @param initial_vel_ Velocity until initial position is reached.
     * @param period_ Rate in milliseconds at which the run() method is called.
     */
    Input(std::vector<Joint> joints, int mode_, bool use_radian_, double initial_vel_, int period_) : mode(mode_), use_radian(use_radian_), period(period_ / 1000.0), RateThread(period_)
    {
        // fill the `parts` map
        for (size_t i = 0; i < joints.size(); ++i)
        {
            // joint appears at ith position in config file
            parts[joints[i].part].push_back({i, joints[i].joint});
        }
        
        if (initial_vel_ == NO_INITIAL_VEL)
            moving_to_initial_pos = DONE;
        else
            moving_to_initial_pos = NOT_STARTED;
        
        if (use_radian)
            initial_vel = initial_vel_ * RAD2DEG;
        else
            initial_vel = initial_vel_;
    }
    
    /**
     * Set control modes of the joints used to the `mode` specified in config.
     * This has to be called before starting the RateThread.
     */
    bool setControlModes()
    {
        return setControlModes(mode);
    }
    
    /**
     * Set control modes of the joints used to a specific mode.
     * @param mode Constant representing control mode.
     */
    bool setControlModes(int mode)
    {
        bool ok = true;
        for (auto& entry : parts)
        {
            auto& part = entry.first;
            for (auto& joint : entry.second)
            {
                ok = ok && controlmode[part]->setControlMode(joint.second, mode);
            }
        }
        return ok;
    }
    
    /**
     * Send commands to robot, depending on the control mode.
     * This has to be called by a child class's run() method.
     * @param v The child class is responsible for reading the Vector from a port/file.
     */
    bool action(Vector& v)
    {
        bool check = true;
        if (use_radian)
        {
            // convert radians to degrees
            for (size_t i = 0; i < v.size(); ++i)
            {
                v[i] = v[i] * RAD2DEG;
            }
        }
        if (moving_to_initial_pos == NOT_STARTED)
        {
            // the initial position is stored in v
            std::cout << "Moving to initial position" << std::endl;
            moving_to_initial_pos = MOVING;
            check = check && setControlModes(VOCAB_CM_POSITION);
            
            for (auto& entry : parts)
            {
                auto& part = entry.first;
                std::vector<int> joints; // the joints we want to send commands to
                std::vector<double> refs; // the data the robot should receive
                for (auto& joint : entry.second)
                {
                    joints.push_back(joint.second);
                    refs.push_back(v[joint.first]);
                }
                
                // set reference speed for all joints to initial_vel
                check = check && pos[part]->setRefSpeeds(joints.size(), &joints[0], &Vector(joints.size(), initial_vel)[0]);
                // move to initial position
                check = check && pos[part]->positionMove(joints.size(), &joints[0], &refs[0]);
            }
        }
        else if (moving_to_initial_pos == MOVING)
        {
            bool done = true;
            for (auto& entry : parts)
            {
                auto& part = entry.first;
                for (auto& joint : entry.second)
                {
                    bool motion_done;
                    check = check && pos[part]->checkMotionDone(joint.second, &motion_done);
                    done = done && motion_done;
                }
            }
            // if done == true, all joints have reached the initial position
            if (done)
            {
                std::cout << "Reached inital position" << std::endl;
                moving_to_initial_pos = REPEAT_FIRST_POSITION;
                check = check && setControlModes();
            }
        }
        else
        {
            if (moving_to_initial_pos == REPEAT_FIRST_POSITION)
                // first position has been repeated using normal control mode
                moving_to_initial_pos = DONE;
            for (auto& entry : parts)
            {
                auto& part = entry.first;
                std::vector<int> joints; // the joints we want to send commands to
                std::vector<double> refs; // the data the robot should receive
                for (auto& joint : entry.second)
                {
                    joints.push_back(joint.second);
                    refs.push_back(v[joint.first]);
                }
                
                switch (mode)
                {
                    // depending on the control mode, use the appropriate driver to sent commands
                    case VOCAB_CM_POSITION:
                    {
                        int ax;
                        check = check && enc[part]->getAxes(&ax);
                        //std::cout << check;
                        Vector encoders(ax);
                        // retrieve current position 
                        check = check && enc[part]->getEncoders(encoders.data());
                        Vector velocity(joints.size());
                        for (size_t i = 0; i < joints.size(); ++i)
                        {
                            // calculate distance between current position
                            // and desired position (the one given in `v`)
                            auto distance = std::abs(refs[i] - encoders[joints[i]]);
                            velocity[i] = distance / period;
                        }
                        check = check && pos[part]->setRefSpeeds(joints.size(), &joints[0], &velocity[0]);
                        check = check && pos[part]->positionMove(joints.size(), &joints[0], &refs[0]);
                        break;
                    }
                    case VOCAB_CM_POSITION_DIRECT:
                        check = check && posdirect[part]->setPositions(joints.size(), &joints[0], &refs[0]);
                        break;
                    case VOCAB_CM_VELOCITY:
                        check = check && vel[part]->velocityMove(joints.size(), &joints[0], &refs[0]);
                        break;
                    case VOCAB_CM_TORQUE:
                        check = check && torque[part]->setRefTorques(joints.size(), &joints[0], &refs[0]);
                        break;
                    case VOCAB_CM_PWM:
                        // there is no function to set duty cycle of a subset of joints
                        for (size_t i = 0; i < joints.size(); ++i)
                        {
                            check = check && pwm[part]->setRefDutyCycle(joints[i], refs[i]);
                        }
                        break;
                
                }
            }
        }
        return check;
    }
    
private:
    int mode; ///< Control mode to use, represented by VOCAB_CM_...
    bool use_radian; ///< Whether to use radian instead of degree
    /**
     * Map: partname -> pair<index in Vector v (according to the order in config file), joint>
     */
    std::map<std::string, std::vector<std::pair<size_t, size_t>>> parts;
    double period; ///< Period in seconds
    /**
     * Velocity until initial position is reached.
     * Set to NO_INITIAL_VEL if not specified in config
     * or one the initial position is reached.
     */
    double initial_vel;
    
protected:
    /**
     * Status of movement to initial position.
     */
    InitialPosStatus moving_to_initial_pos;
};
        
/**
 * Periodically read from the file specified by `filename`
 * and move motors accordingly.
 */
class InputFile : public Input
{
public:
    /**
     * @param filename_ File to read from.
     * @param joints The joints to move when action() is called. They should be in the same
     *               order as in the config file.
     * @param mode Constant representing control mode.
     * @param use_radian Whether to use radian instead of degree.
     *                   If set to true, the input will be converted from degrees to radians.
     *                   Makes no sense when using PWM control.
     * @param initial_vel Velocity until initial position is reached.
     * @param period Rate in milliseconds at which the run() method is called.
     */
    InputFile(std::string filename_, std::vector<Joint> joints, int mode, bool use_radian, double initial_vel, int period) : filename(filename_), stream(filename_), v(joints.size()), Input(joints, mode, use_radian, initial_vel, period)
    {
        if (!stream.is_open())
        {
            std::cerr << "Could not open file " << filename << std::endl;
            std::exit(1);
        }
    }
    
    /**
     * Read the next n values from file, where `n = joints.size()`.
     * Then, action() is called with this data. 
     * This is called every `period` ms by RateThread.
     */
    virtual void run()
    {
        bool ok = true;
        if (moving_to_initial_pos == MOVING)
        {
            // check if motion done
            ok = action(v);
        }
        else if (moving_to_initial_pos == REPEAT_FIRST_POSITION)
        {
            std::cout << Time::now() << std::endl;
            if (start_read)
            {
                Bottle b;
                b.addString("start");
                readRPC.write(b);
                start_read = false;
            }
            // repeat the first line from file
            ok = action(v);
        }
        else //moving_to_initial_pos == NOT_STARTED || moving_to_initial_pos == DONE
        {
            if (start_read && moving_to_initial_pos == DONE)
            {
                Bottle b;
                b.addString("start");
                readRPC.write(b);
                start_read = false;
            }
            std::string line;
            // read.cpp puts a \n at the end of a file, therefore we have to check for an empty line
            if (!std::getline(stream, line) || line == "")
            {
                std::cout << "Reached end of file: " << filename << std::endl;
                stop(); // stop RateThread
                stream.close(); 
                if (stop_read)
                {
                    Bottle b;
                    b.addString("stop");
                    readRPC.write(b);
                }
                return;
            }
            
            std::istringstream line_stream(line);
            size_t i = 0;
            std::string cell;
            while (std::getline(line_stream, cell, ',')) // read cells seperated by commas
            {
                if (i > v.size())
                {
                    std::cerr << "Too many values in one line in file: " << filename << std::endl;
                    std::exit(1);
                }
                v[i] = atof(cell.c_str()); // atof: convert string to double
                ++i;
            }
            if (i < v.size())
            {
                std::cerr << "Too few values in one line in file: " << filename << std::endl;
                std::exit(1);
            }
            
            ok = action(v); // call action() from Input class
        }
        if (!ok)
        {
            std::cerr << "Could not move motors." << std::endl;
            std::exit(1);
        }
    }
    
private:
    std::string filename; ///< File to read commands from.
    std::ifstream stream; ///< Input stream of file to read the commands from.
    Vector v; ///< Vector containing data of one line from file.
};

/**
 * Periodically read from the port specified by `portname`
 * and move motors accordingly.
 */
class InputPort : public Input
{
public:
    /**
     * @param portname Port to read from.
     * @param joints The joints to move when action() is called. They should be in the same
     *               order as in the config file.
     * @param mode Constant representing control mode.
     * @param use_radian Whether to use radian instead of degree.
     *                   If set to true, the input will be converted from degrees to radians.
     *                   Makes no sense when using PWM control.
     * @param initial_vel Velocity until initial position is reached.
     * @param period Rate in milliseconds at which the run() method is called.
     */
    InputPort(std::string portname, std::vector<Joint> joints, int mode, bool use_radian, double initial_vel, int period) : Input(joints, mode, use_radian, initial_vel, period)
    {
        port.open(portname);
        if (port.isClosed())
        {
            std::cerr << "Could not open port " << portname << std::endl;
            std::exit(1);
        }
    }
    
    /**
     * Read the next vector from port.
     * Then, action() is called with this data. 
     * This is called every `period` ms by RateThread.
     */
    virtual void run()
    {
        bool ok = true;
        if (moving_to_initial_pos == MOVING)
        {
            // check if motion done
            ok = action(v);
        }
        else if (moving_to_initial_pos == REPEAT_FIRST_POSITION)
        {
            if (start_read)
            {
                Bottle b;
                b.addString("start");
                readRPC.write(b);
                start_read = false;
            }
            // repeat the vector received first
            ok = action(v);
        }
        else //moving_to_initial_pos == NOT_STARTED || moving_to_initial_pos == DONE
        {
            if (start_read && moving_to_initial_pos == DONE)
            {
                Bottle b;
                b.addString("start");
                readRPC.write(b);
                start_read = false;
            }
            Vector* received = port.read(false); // false: doesn't wait for data to arrive
            if (received != YARP_NULLPTR) // check if data was available
            {
                v = *received;
                ok = action(v); // call action() from Input class
            }
        }
        if (!ok)
        {
            std::cerr << "Could not move motors." << std::endl;
            std::exit(1);
        }
    }
    
private:
    BufferedPort<Vector> port; ///< Port to read the commands from.
    Vector v; ///< Vector containing data last received.
};

// store all instances of InputFile/InputPort, so we can start() them later
std::vector<InputFile*> files;
std::vector<InputPort*> ports;

class RpcPort : public BufferedPort<Bottle>
{
public:
    RpcPort()
    {
        useCallback();
    }
    
    using BufferedPort<Bottle>::onRead;
    virtual void onRead(Bottle& b)
    {
        std::string cmd = b.toString();
        
        if (cmd == "stop")
        {
            for (auto& port : ports)
            {
                port->stop();
            }
            for (auto& file : files)
            {
                file->stop();
            }
        }
        else if (cmd == "start")
        {
            for (auto& port : ports)
            {
                port->start();
            }
            for (auto& file : files)
            {
                file->start();
            }
        }
        else
        {
            std::cerr << "Unknown RPC command: " << cmd << std::endl;
        }
    }
};

/**
 * - Read --robot command line argument
 * - Read write section of config file and create InputPort and InputFile objects
 * - Set up IPositionControl2, ... objects
 * - Start RateThreads
 */
int main(int argc, char *argv[])
{
    Network yarp;
    Property params;
    params.fromCommand(argc, argv);
    
    if (!params.check("robot"))
    {
        std::cerr << "Please specify the name of the robot" << std::endl;
        std::cerr << "--robot name (e.g. icub)" << std::endl;
        std::exit(1);
    }
    std::string robotName = params.find("robot").asString().c_str();
    
    // for each part we need to store the modes that are used in order to create the appropriate drivers
    std::map<std::string,std::set<int>> accessed_modes; // map: part -> modes
    
    std::string configName;
    if (params.check("config"))
        configName = params.find("config").asString().c_str();
    else
        configName = "configuration.yaml";
    YAML::Node config;
    try
    {
        config = YAML::LoadFile(configName);
    }
    catch (YAML::BadFile e)
    {
        std::cerr << "Could not open file " << configName << std::endl;
        std::exit(1);
    }

    // iterate over all entries in the write section of the config file
    for (YAML::const_iterator write = config["write"].begin(); 
         write != config["write"].end();
         ++write) 
    {
        std::string modename = (*write)["mode"].as<std::string>();
        int mode; // store constant representing the control mode
        bool use_radian = false;
        if ((*write)["unit"])
        {
            std::string unit = (*write)["unit"].as<std::string>();
            if (unit == "radian")
                use_radian = true;
            else if (unit == "degree")
                use_radian = false;
            else
            {
                std::cerr << "Unknown unit: " << unit << std::endl;
                std::exit(1);
            }
        }
        
        double initial_vel = NO_INITIAL_VEL;
        if ((*write)["initial_velocity"])
            initial_vel = (*write)["initial_velocity"].as<double>();
        
        if (modename == "position")
        {
            mode = VOCAB_CM_POSITION;
        }
        else if (modename == "position_direct")
        {
            mode = VOCAB_CM_POSITION_DIRECT;
        }
        else if (modename == "velocity")
        {
            mode = VOCAB_CM_VELOCITY;
        }
        else if (modename == "torque")
        {
            mode = VOCAB_CM_TORQUE;
        }
        else if (modename == "pwm")
        {
            mode = VOCAB_CM_PWM;
        }
        else
        {
            std::cerr << "Unknown mode: " << modename << std::endl;
            std::exit(1);
        }
        
        std::vector<Joint> joints;
        // iterate over motors and add joints to `joints`
        for (YAML::const_iterator motor = (*write)["motors"].begin();
             motor != (*write)["motors"].end();
             ++motor)
        {
            std::string part = (*motor)["part"].as<std::string>();
            accessed_modes[part].insert(mode);
            if (mode == VOCAB_CM_POSITION_DIRECT)
            {
                // needed to reach the initial position
                accessed_modes[part].insert(VOCAB_CM_POSITION);
            }
            if ((*motor)["joint"]) // single joint
            {
                joints.push_back(Joint{
                    part,
                    (*motor)["joint"].as<size_t>()
                });
            }
            else // joints: [ ... ]
            {
                for (YAML::const_iterator joint = (*motor)["joints"].begin();
                        joint != (*motor)["joints"].end();
                        ++joint)
                {
                    joints.push_back(Joint{
                        part,
                        joint->as<size_t>()
                    });
                }
            }
        }
        
        // create InputFile and InputPort objects
        int period = (*write)["period"].as<int>(); // in milliseconds
        if ((*write)["file"] && (*write)["port"])
        {
            std::cerr << "It's not allowed to specify both, file and port." << std::endl;
            std::exit(1);
        }
        else if ((*write)["file"])
        {
            InputFile* file = new InputFile(
                (*write)["file"].as<std::string>(), 
                joints, mode, use_radian, initial_vel, period);
            files.push_back(file);
        }
        else if ((*write)["port"])
        {
            InputPort* port = new InputPort(
                (*write)["port"].as<std::string>(), 
                joints, mode, use_radian, initial_vel, period);
            ports.push_back(port);
        }
        else
        {
            std::cerr << "You have to specify a file or a port as input." << std::endl;
            std::exit(1);
        }
    }
    
    // create the necessary drivers and store them in the global maps defined at the top
    for (auto& entry : accessed_modes)
    {
        auto& part = entry.first;
        std::string remotePorts = "/" + robotName + "/" + part;
        std::string localPorts = "/client_write/" + part;
        Property options;
        options.put("device", "remote_controlboard");
        options.put("local", localPorts.c_str());   //local port names
        options.put("remote", remotePorts.c_str());
        
        PolyDriver* robotDevice = new PolyDriver(options);
        if (!robotDevice->isValid()) 
        {
            std::cerr <<"Device not available.  Here are the known devices:" << std::endl;
            std::cout << Drivers::factory().toString().c_str();
            std::exit(1);
        }
        
        auto& modes = entry.second; // all control modes that are used with this part
        bool ok = true;
        IControlMode2* control;
        ok = ok && robotDevice->view(control);
        controlmode[part] = control;
        if (modes.find(VOCAB_CM_POSITION) != modes.end()) // if modes contains position
        {
            IPositionControl2* p;
            ok = ok && robotDevice->view(p); // view() creates an IPositionControl2 object and sets `p` to its address
            pos[part] = p;
            
            IEncoders* e; 
            ok = ok && robotDevice->view(e);
            enc[part] = e;
        }
        if (modes.find(VOCAB_CM_POSITION_DIRECT) != modes.end()) // if modes contains position_direct
        {
            IPositionDirect* direct;
            ok = ok && robotDevice->view(direct);
            posdirect[part] = direct;
        }
        if (modes.find(VOCAB_CM_VELOCITY) != modes.end())
        {
            IVelocityControl2* velocity;
            ok = ok && robotDevice->view(velocity);
            vel[part] = velocity;
        }
        if (modes.find(VOCAB_CM_TORQUE) != modes.end())
        {
            ITorqueControl* tor;
            ok = ok && robotDevice->view(tor);
            torque[part] = tor;
        }
        if (modes.find(VOCAB_CM_PWM) != modes.end())
        {
            IPWMControl* pwmc;
            ok = ok && robotDevice->view(pwmc);
            pwm[part] = pwmc;
        }
        if (!ok) 
        {
            std::cerr << "Problems acquiring interfaces" << std::endl;
            std::exit(1);
        }
    }
    
    start_read = false; // send start command to read's RPC port
    stop_read = false; // send stop command when end of file is reached
    if (params.check("start-read"))
    {
        start_read = true;
        stop_read = true;
        readRPC.open("/write/rpc_client");
        Network::connect("/write/rpc_client", "/read/rpc");
    }
    
    // set control modes and start RateThreads
    bool ok = true;
    for (auto& port : ports)
    {
        ok = ok && port->setControlModes();
        port->start();
    }
    for (auto& file : files)
    {
        ok = ok && file->setControlModes();
        file->start();
    }
    if (!ok)
    {
        std::cerr << "Unable to set control modes" << std::endl;
        std::exit(1);
    }
    
    RpcPort r;
    r.open("/write/rpc");
    
    pause();
}
